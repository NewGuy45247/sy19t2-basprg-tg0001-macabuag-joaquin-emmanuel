#include <conio.h>
#include <time.h>
#include <iostream>

using namespace std;

int main()
{
	srand(time(0));

	//Player stats
	int playerMaxhp;
	cout << "pls input Sebastian's maxHp (used for equations)" << endl;
	cin >> playerMaxhp;

	int playerHp;
	cout << "pls input Sebastian's hp" << endl;
	cin >> playerHp;

	int playerMindamage;
	cout << "pls input Sebastian's minDamage" << endl;
	cin >> playerMindamage;

	int playerMaxdamage;
	cout << "pls input Sebastian's maxDamage" << endl;
	cin >> playerMaxdamage;

	//Dragon stats
	int dragonMaxhp;
	cout << "pls input Dragon's maxHp (used for equations)" << endl;
	cin >> dragonMaxhp;

	int dragonHp;
	cout << "pls input Dragon's hp" << endl;
	cin >> dragonHp;

	int dragonMindamage;
	cout << "pls input Dragon's minDamage" << endl;
	cin >> dragonMindamage;

	int dragonMaxdamage;
	cout << "pls input Dragon's maxDamage" << endl;
	cin >> dragonMaxdamage;

	system("CLS");

	while (true)
	{
		// computations and shiz
		int dragonChoice = 1 + (rand() % 3);
		// player hp and damage equations
		int playerDamage = (rand() % (dragonMindamage - dragonMaxdamage + 1)) + dragonMindamage;
		int playerWildamage = ((rand() % (dragonMindamage - dragonMaxdamage + 1)) + dragonMindamage) * 2;
		float playerPercent = ((float)playerHp / playerMaxhp) * 100.f;
		// dragon hp and damage equations
		int dragonDamage = (rand() % (playerMindamage - playerMaxdamage + 1)) + playerMindamage;
		int dragonWildamage = ((rand() % (playerMindamage - playerMaxdamage + 1)) + playerMindamage) * 2;
		int	dragonDefend = playerDamage / 2;
		float dragonPercent = ((float)dragonHp / dragonMaxhp) * 100.f;
		int playerDefend = dragonDamage / 2;

		//choices
		cout << "1: Sword Attack" << endl;
		cout << "2: Guard Stance" << endl;
		cout << "3: Sebastian Slam!" << endl;
		cout << "4: Quit the Game and run away like a Pu&&y" << endl;

		int playerChoice;
		cout << "The dragon steels itself as it awaits Sebastian's next attack" << endl;
		cin >> playerChoice;

		cout << dragonChoice << endl;

		// you attacked!
		if (playerChoice == 1)
		{
			if (dragonChoice == 1)
			{
				cout << "you've traded blows!" << endl;

				cout << "damaged, your hp is now " << ((playerHp -= dragonDamage) = playerHp) << endl;
				cout << "dragon's hp is now " << ((dragonHp -= playerDamage) = dragonHp) << endl;

				cout << "damage recieved: " << dragonDamage << endl;
				cout << "damage given: " << playerDamage << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerHp << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;

				if (playerPercent <= 0.f && dragonPercent > 0.f)
				{
					//lose condition
					cout << "I'm sorry...";
					_getch();
					cout << "friend" << endl;
					_getch();
					return 0;
				}
				else if (playerPercent > 0.f && dragonPercent <= 0.f)
				{
					//win condition
					cout << "It's time...";
					_getch();
					cout << "I have fulfilled my mission..." << endl;
					_getch();
					cout << "friend...";
					_getch();
					cout << "Time to keep my promise..." << endl;
					_getch();
					cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
					cout << "Fin" << endl;
					_getch();
					return 0;
				}
			}

			else if (dragonChoice == 2)
			{
				cout << "The Dragon activated PLatinum Hide!" << endl;

				cout << "The dragon activated Platinum hide! and his hp is now " << ((dragonHp -= dragonDefend) = dragonHp) << endl;

				cout << "dragon's defense is " << dragonDefend << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerPercent << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}
			}

			else if (dragonChoice == 3)
			{
				cout << "Sebastian managed to attack the Dragon, but got wounded by the Dragon's FrostBreath!" << endl;

				cout << "you've been significantly damaged! your hp is now " << ((playerHp -= dragonWildamage) = playerHp) << endl;
				cout << "dragon's hp is now " << ((dragonHp -= playerDamage) = dragonHp) << endl;

				cout << "damage recieved: " << dragonWildamage << endl;
				cout << "damage given" << playerDamage << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerPercent << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}

			}
		}

		// you blocked!
		if (playerChoice == 2)
		{
			if (dragonChoice == 1)
			{
				cout << "Sebastian anticipates the Dragon's next move..." << endl;
				_getch();
				cout << "Sebastian blocks against the dragon's claw attacks in rapid succession!" << endl;

				cout << "damaged, your hp is now " << ((playerHp -= playerDefend) = playerHp) << endl;

				cout << "your defense is " << playerDefend << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerPercent << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}
			}

			else if (dragonChoice == 2)
			{
				cout << "Sebastian anticipates the Dragon's next move..." << endl;
				_getch();
				cout << "Both Sebastian and the Dragon took on a defensive stance!" << endl;
				_getch();
				cout << "nothing happened..." << endl;
				_getch();
				system("CLS");
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}
			}

			else if (dragonChoice == 3)
			{
				cout << "Sebastian anticipates the Dragon's next move..." << endl;
				_getch();
				cout << "As the dragon starts to unleash its Frostbreath, Sebastian attacks the Dragon with all his might!" << endl;

				cout << "dragon's hp is now " << ((dragonHp -= playerWildamage) = dragonHp) << endl;

				cout << "damage given: " << playerWildamage << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerPercent << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}

			}
		}

		// you gave it your all
		if (playerChoice == 3)
		{
			if (dragonChoice == 1)
			{
				cout << "The Monique secret technique..." << endl;
				_getch();
				cout << "Sebastian Slam!" << endl;
				_getch();
				cout << "you've tanked through the dragon's claw attack!" << endl;

				cout << "damaged, your hp is now " << ((playerHp -= dragonDamage) = playerHp) << endl;
				cout << "dragon's hp is now " << ((dragonHp -= playerWildamage) = dragonHp) << endl;

				cout << "damage recieved: " << dragonDamage << endl;
				cout << "damage given: " << playerWildamage << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerPercent << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}
			}

			else if (dragonChoice == 2)
			{
				cout << "The Monique secret technique..." << endl;
				_getch();
				cout << "Sebastian Slam!" << endl;
				_getch();
				cout << "Sebastian gave it his all... but the Dragon had clawed him right before he swung his blade!" << endl;

				cout << "significantly damaged! your hp is now " << ((playerHp -= dragonWildamage) = playerHp) << endl;

				cout << "damage recieved: " << dragonWildamage << " %" << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerPercent << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}
			}

			else if (dragonChoice == 3)
			{
				cout << "The Monique secret technique..." << endl;
				_getch();
				cout << "Sebastian Slam!" << endl;
				_getch();
				cout << "Sebastian Tanks through the Dragon's Frostbreath attack and both recieve massive damage!" << endl;

				cout << "you've been significantly damaged! your hp is now " << ((playerHp -= dragonWildamage) = playerHp) << endl;
				cout << "singnificantly damaged, his hp is now " << ((dragonHp -= playerWildamage) = dragonHp) << endl;

				cout << "damage recieved: " << dragonWildamage << endl;
				cout << "damage given " << playerWildamage << endl;
				_getch();
				system("CLS");
				cout << "Your health is at " << playerPercent << " %" << endl;
				cout << "dragon health is at " << dragonPercent << " %" << endl;
				if (playerPercent < 0.f || dragonPercent < 0.f)

					if (playerPercent <= 0.f && dragonPercent > 0.f)
					{
						//lose condition
						cout << "I'm sorry...";
						_getch();
						cout << "friend" << endl;
						_getch();
						return 0;
					}
					else if (playerPercent > 0.f && dragonPercent <= 0.f)
					{
						//win condition
						cout << "It's time...";
						_getch();
						cout << "I have fulfilled my mission..." << endl;
						_getch();
						cout << "friend...";
						_getch();
						cout << "Time to keep my promise..." << endl;
						_getch();
						cout << "Sebastian Sheathes his sword on his back and walks back to the town he came from and uphold his promise to meet with a dear friend of his" << endl;
						cout << "Fin" << endl;
						_getch();
						return 0;
					}

			}

		}

		// player quits
		if (playerChoice == 4)
		{
			cout << "Sebastian ran from the fight" << endl;
			_getch();
			cout << "NIGERUNDAYYOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO! he said" << endl;
			_getch();
			cout << "The Fight ended then and there with the dragon awkwardly sitting in his den" << endl;
			return 0;
		}
	}
}