#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

void pause()
{
	//just a shortcut
	_getch();
}

void thankYousir()
{
	cout << "Sir, I am grateful for the lessons learned in BASPRG - 01 " << endl;
	pause();
	cout << "I still have a long path of growth ahead of me" << endl;
	pause();
	cout << "Especially when it comes to programming!" << endl;
	pause();
	cout << "I still have to finish the exam, so please wait for the remaining two" << endl;
	system("pause");
}


int main()
{
	srand(time(0));
	int items[100] = { 1 };
	int arraySize = 100;
	int missing = 1;
	int x = 1;

	//generating random numbers
	for (int i = 0; i < arraySize; i++)
	{
		items[i] = x++;
		cout << items[i] << endl;
		missing = items[rand() % 99 + 1];
	}

	//was unable to find out how to make the missing number invisible
	cout << endl;
	cout << "the missing number is ";
	cout << missing;

	pause();
	system("CLS");
	thankYousir();

	return 0;
}

