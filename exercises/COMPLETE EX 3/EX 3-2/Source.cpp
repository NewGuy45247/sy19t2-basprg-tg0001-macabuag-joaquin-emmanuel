#include<iostream>
#include<conio.h>

using namespace std;

int main()
{
	int value = 1;
	int factorial = 1;
	cout << " Type in a number and I'll find the factorial for you:  ";
	cin >> value;

	for (int a = 1; a <= value; a++)
	{
		factorial = factorial * a;
	}

	cout << "Factorial of " << value << " is " << factorial << endl;
	system("pause");
	return 0;
}