#include<iostream>
#include<conio.h>

using namespace std;

int main()
{
	cout << "Print odd numbers" << endl;
	int maxNum;
	cin >> maxNum;
	

	system("CLS");

	int i;

	// I initially thought that I could use the / operator for dividing but after watching a video (telling that I should use modulus) I realized that
	// modulus/ % was better for finding odd numbers in order to see if there is a remainder or not

	for (i = 1; i <= maxNum; i++)
	{
	int remainder = i % 2;
		if (remainder == 1)
		{
			cout << i << endl;
		}
		else
		{
			cout << endl;
		}
	}
	system("pause");
	return 0;
}