#include <iostream>
#include <conio.h>

using namespace std;

int main()
{

	int currentMorale;
	int maxMorale;

	cout << "On a scale of 1 being the saddest, and 1000 being the happiest, at what number are you most happy?" << endl;
	cin >> maxMorale;
	cout << "How about your current state of mind?" << endl;
	cin >> currentMorale;
	system("CLS");

	float percent = (float)currentMorale / maxMorale;
	cout << percent * 100.0f << endl;
	system("CLS");

	if (currentMorale == 1.0f)
	{
		cout << "Glad to know you're having a good day! I am proud of you on who you are and where you have been" << endl;
	}

	else if (currentMorale <= 0.5f)
	{
		cout << "Hmmm... good to know... At least you're improving! So get out there a " << endl;
	}

	else if (percent <= 0.3f && percent <= 0.5f)
	{
		cout << "Hey... No matter what you must not let stress or people bring you down, so please stop making problems and start finding solutions" << endl;
	}


	else if (percent <= 0.3 && percent >= 0)
	{
		cout << "HEY! YOU!" << endl;
		_getch();
		cout << "YES YOU!" << endl;
		_getch();
		cout << "JUST DO IT!" << endl;
		_getch();
		cout << "DON'T LET YOUR DREAMS BE DREAMS!" << endl;
		_getch();
		cout << "SO JUST DO IT" << endl;
		_getch();
	}
}