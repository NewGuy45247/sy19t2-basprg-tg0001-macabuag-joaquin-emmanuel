#include <iostream>
#include <Windows.h>
#include <conio.h>

using namespace std;

int main()
{
//declaration
//played with declaration and variables for a bit
	int first[] = { 1, 2, 3, 4, 5 };
	int second[] = { 0, 7, -10, 5, 3 };
	int sum[5];

//show user the values
	cout << "numbers1 array consists of " << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << first[i] << endl;
	}

	cout << endl;
	cout << "numbers2 array consists of " << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << second[i] << endl;
	}
	_getch();

//show user the answers
	system("CLS");
	cout << "Output" << endl;
	for (int i = 0; i < 5; i++)
	{
		sum[i] = first[i] + second[i];
		cout << sum[i] << endl;
	}
	return 0;
}