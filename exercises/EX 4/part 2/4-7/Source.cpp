#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;


int main()
{
    int numbers[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };

    int largest = INT_MIN;

    for (int i = 0; i < 10; i++)
    {
        if (numbers[i] > largest)
        {
            largest = numbers[i];
        }
    }
    cout << largest << " is the largest number" << endl;
    
    //Maybe if I done this earlier, I wouldn't have had to look at the answer key
    //I don't think I could've found a simpler solution either way
    return 0;
}
