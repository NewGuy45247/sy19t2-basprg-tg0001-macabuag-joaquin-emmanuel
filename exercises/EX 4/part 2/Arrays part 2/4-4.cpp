#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;


int main()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf",
		"Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	string find;

	//input
	while (find != "end")
	{
		cout << endl;
		cout << "Type Display to display inventory" << endl;
		cout << "Type end to end program" << endl;
		cout << "Or just type Inventory item to find" << endl;
		cin >> find;
		system("CLS");

		int found = 1;
		//Checking
		for (int i = 0; i < arraySize; i++)
		{
			if (find == items[i])
			{
				found = i;
				break;
			}
		}

		if (find == "Display")
		{
			//Display
			for (int i = 0; i < arraySize; i++)
			{
				cout << items[i] << endl;
			}
			_getch();
			system("CLS");
		}
		else
		{
			if (found >= 0)
			{
				cout << find << " This is item Number " << found << endl;
			}
			else
			{
				cout << find << " is not part of the list" << endl;
			}
		}
	}

	system("pause");
	return 0;
}