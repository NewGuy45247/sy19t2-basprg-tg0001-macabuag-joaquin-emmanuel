#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

int main()
{
	string firstName;
	string lastName;
	int Age;
	int amount;
	bool breakfast;

	cout << "NOTE" << std::endl;
	cout << "i did not use the system (pause) funtion" << std::endl;
	cout << "so just press enter to continue the short experience of my programming exercise" << std::endl;
	cout << "enjoy!" << std::endl;
	system("pause");
	system("CLS");

	cout << "Hello there!" << std::endl;
	_getch();
	cout << "My name's Sebastian ";
	_getch();
	cout << "Manuel ";
	_getch();
	cout << "Marian ";
	_getch();
	cout << "Marquez ";
	_getch();
	cout << "Monique!" << std::endl;
	_getch();
	cout << "what's your name my friend?" << std::endl;
	cin >> firstName;
	system("CLS");

	cout << "Such a beautiful name!" << std::endl;
	_getch();
	cout << "how 'bout your family name then?" << std::endl;
	cin >> lastName;
	system("CLS");
	cout << "I see... ";
	_getch();
	cout << "Never heard one such as that before!" << std::endl;
	_getch();

	cout << "I was born in the far western lands of Charlingayl in 1999 and am 24 years old! ";
	_getch();
	cout << "Now of what age are you " << firstName << "?" << std::endl;
	cin >> Age;
	system("CLS");

	cout << "good to know that we are both of the same youth!" << std::endl;
	system("CLS");

	cout << "by the way, have you eaten breakfast ? (type 1 for yes or 0 for no)" << std::endl;
	cin >> breakfast;
	system("CLS");

	cout << "I see" << std::endl;
	_getch();
	system("CLS");

	cout << "anyways, its been nice having a small chat with you!" << std::endl;
	_getch();
	system("CLS");

	cout << "I hope that one day we shall cross paths again my friend" << std::endl;
	_getch();
	system("CLS");

	cout << "See you next time " << firstName << std::endl;
	_getch();
	cout << "its a pleasure meeting you" << std::endl;

	system("pause");
	return 0;
}