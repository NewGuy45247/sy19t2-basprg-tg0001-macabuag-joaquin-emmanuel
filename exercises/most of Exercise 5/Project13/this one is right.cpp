#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

int command()
{
	//declaration
	int value = 1;
	int factorial = 1;
	cout << " Type in a number and I'll find the factorial for you:  ";
	cin >> value;

	for (int a = 1; a <= value; a++)
	{
		factorial = factorial * a;
	}

	cout << "Factorial of " << value << " is " << factorial << endl;
	return 0;
}

int main()
{
	//starting up just to make sure that this happens first
	cout << "Starting..." << endl;
	_getch();
	command();
}